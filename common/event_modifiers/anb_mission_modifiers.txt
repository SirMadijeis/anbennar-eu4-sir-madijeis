
generic_into_anbennar = {
	ae_impact = -0.2
	manpower_recovery_speed = 0.1
}

generic_into_lorent = {
	ae_impact = -0.2
	manpower_recovery_speed = 0.1
}

lorent_lorentish_naval_drills = {
	galley_power = 0.25
	naval_maintenance_modifier = -0.1
}

lorent_reformed_knights_of_the_rose = {
	cavalry_power = 0.15
}

lorent_emperor_disregards_lorentish_advances = {
	ae_impact = -0.2
}

lorent_ruby_crown_awakened = {
	yearly_absolutism = 0.1
	legitimacy = 1
	monarch_admin_power = 1
	monarch_diplomatic_power = 1
	monarch_military_power = 1
}

gawed_gawedi_settlers = {
	colonists = 1
	colonist_placement_chance = 0.05
}

overclan_rebuild_sur-els_temple = {
	religious_unity = 0.20
	tolerance_heathen = 1
	religion = yes
}

burning_of_the_redglades = {
	local_development_cost = 1
	local_production_efficiency = -0.5
}

bane_of_elfkind = {
	unjustified_demands = -0.2
}


#Eborthil Mission modifiers

# Stonegaze toll
stonegaze_toll_modifier = {
	local_institution_spread = 0.10
	province_trade_power_value = 10
	naval_forcelimit = 2
	local_development_cost = -0.05
	local_sailors_modifier = 0.50
	picture = "province_trade_power_value"
}

#Ourdian Mission modifiers. 

ourdi_protected_trade = {
	global_own_trade_power = 0.15
	trade_efficiency = 0.1
	ship_power_propagation = 0.1
}

ourdi_settlers_stream = {
	culture_conversion_cost = -0.1
	missionary_strength = 0.01
}

ourdi_crusading_zeal = {
	tolerance_own = 1
	tolerance_heathen = -1
	missionary_strength = 0.01
}

ourdi_favoured_by_corin = {
	land_morale = 0.1
	naval_morale = 0.1
	discipline = 0.075
	global_unrest = -2
}

ourdi_empire_of_men = {
	core_creation_cost = -0.20
	global_unrest = -2
	tolerance_heathen = -4
}

ourdi_colonial_fervor = {
	colonist_placement_chance = 0.05
	global_colonial_growth = 20
}
	
ourdi_awakening_the_economy = {
	development_cost = -0.1
	production_efficiency = 0.1
}

ourdi_bustling_trade = {
	global_trade_power = 0.15
	ship_power_propagation = 0.1
	naval_maintenance_modifier = -0.1
}

ourdi_dostanorian_unity = {
	global_unrest = -2
	prestige = 2
	legitimacy = 2
	republican_tradition = 0.25
	devotion = 0.25
}

ourdi_castanorian_lessons = {
	defensiveness = 0.1
	build_cost = -0.1
	build_time = -0.3
	fort_manteinance_modifier = -0.05
}