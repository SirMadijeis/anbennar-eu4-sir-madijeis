# Triggered modifiers are here.
# these are checked for each countries once/month and then applied.
#
# Effects are fully scriptable here.


###########################################
# Discovery of the East Indian trade route.
###########################################
monstrous_nation = {
	potential = {
	}

	trigger = {
			OR = 
			{
				culture_group = kobold
				culture_group = orcish
				culture_group = gnollish
				culture_group = goblinoid
				culture_group = harpy
			}
			NOT = 
			{
				is_year = 1650
			}
	}
	diplomatic_reputation = -1
	unjustified_demands = -0.5

}

elven_nation = {
	potential = {
	}

	trigger = {
			OR = 
			{
				culture_group = elven
			}
			NOT = 
			{
				#something to do "after this year"
			}
	}
	manpower_recovery_speed = -0.5
	global_colonial_growth = -25
	discipline = 0.1
	land_morale = 0.05
	global_tax_modifier = 0.1
	production_efficiency = 0.1
	prestige = 1
	adm_tech_cost_modifier = -0.1
	war_exhaustion_cost = 0.1
}

dwarven_nation = {
	potential = {
	}

	trigger = {
			OR = 
			{
				culture_group = dwarven
			}
			NOT = 
			{
				#something to do "after this year"
			}
	}
	manpower_recovery_speed = -0.25
	global_colonial_growth = -15
	production_efficiency = 0.2
	land_morale = 0.1
	infantry_power = 0.1
	artillery_power = 0.1
	defensiveness = 0.1
	mil_tech_cost_modifier = -0.1
	trade_efficiency = 0.1
	build_time = -0.15
}

gnomish_nation = {
	potential = {
	}

	trigger = {
			OR = 
			{
				culture_group = gnomish
			}
			NOT = 
			{
				#something to do "after this year"
			}
	}
	manpower_recovery_speed = -0.3
	global_colonial_growth = -20
	production_efficiency = 0.15
	land_morale = -0.1
	discipline = -0.1
	hostile_attrition = 1
	technology_cost = -0.1
	global_spy_defence = 0.2
	spy_offence = 0.2
}

halfling_nation = {
	potential = {
	}

	trigger = {
			OR = 
			{
				culture_group = halfling
			}
			NOT = 
			{
				#something to do "after this year"
			}
	}
	manpower_recovery_speed = 0.1
	land_morale = -0.1
	discipline = -0.1
	hostile_attrition = 1
	global_spy_defence = 0.1
	spy_offence = 0.1
	trade_efficiency = 0.05
}

anbennarian_nation = {
	potential = {
	}

	trigger = {
			OR = 
			{
				culture_group = anbennarian
			}
			NOT = 
			{
				#something to do "after this year"
			}
	}
	diplomatic_reputation = 1
}

kobold_nation = {
	potential = {
	}

	trigger = {
			OR = 
			{
				culture_group = kobold
			}
			NOT = 
			{
				#something to do "after this year"
			}
	}
	manpower_recovery_speed = 0.25
	land_morale = -0.1
	discipline = -0.1
	hostile_attrition = 1
}

goblin_nation = {
	potential = {
	}

	trigger = {
			OR = 
			{
				culture_group = goblinoid
			}
			NOT = 
			{
				#something to do "after this year"
			}
	}
	manpower_recovery_speed = 0.20
	land_morale = -0.05
	discipline = -0.05
	land_forcelimit_modifier = 0.10
}


owns_anbenncost = {
	potential = {
	}

	trigger = {
		owns = 8	#Anbenncost
	}
	diplomatic_upkeep = 1	
}


hre_dominant_regent_court = {
	potential = {
		capital_scope = {
			continent = europe
		}
		OR = {
			religion = regent_court
			religion = corinite
		}
	}

	trigger = {
		religion = regent_court
		hre_religion = regent_court
		hre_religion_locked = yes
	}
	
	legitimacy = 0.25
	tolerance_own = 1
	global_missionary_strength = 0.01
	imperial_authority = 0.25
}

hre_dominant_corinite = {
	potential = {
		capital_scope = {
			continent = europe
		}
		OR = {
			religion = regent_court
			religion = corinite
		}
	}

	trigger = {
		religion = corinite
		hre_religion = corinite
		hre_religion_locked = yes
	}
	
	legitimacy = 0.25
	tolerance_own = 1
	global_missionary_strength = 0.01
	imperial_authority = 0.25
}
