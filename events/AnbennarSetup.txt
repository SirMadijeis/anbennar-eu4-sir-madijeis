
namespace = anbennar_setup

#Country Setup for Monstrous Opinions
country_event = {
	id =  anbennar_setup.1
	title =  anbennar_setup.1.t
	desc =  anbennar_setup.1.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	#fire_only_once = yes
	hidden = yes
	is_triggered_only = yes
	
	option = {		
		name = "anbennar_setup.1.a"
		
        if = {
            limit = {
                has_country_modifier = monstrous_nation
            }
			every_country = {	#If you're a monstrous nation you like other monstrous nations
				limit = {
					has_country_modifier = monstrous_nation
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = both_monstrous } 
					}
				}
				add_opinion = { who = ROOT modifier = both_monstrous }
			}
			every_country = {	#If you're not a monstrous nation you hate monstrous nations
				limit = {
					NOT = { has_country_modifier = monstrous_nation }
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = root_monstrous } 
					}
				}
				add_opinion = { who = ROOT modifier = root_monstrous }
			}
		}
		
        if = {
            limit = {
                NOT = { has_country_modifier = monstrous_nation }
            }
			every_country = {	#If you're a monstrous nation you like other monstrous nations
				limit = {
					has_country_modifier = monstrous_nation
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = root_monstrous } 
					}
				}
				add_opinion = { who = ROOT modifier = root_monstrous }
			}
		}
	}
}

#Country Setup for Lilac Wars Parties
country_event = {
	id =  anbennar_setup.2
	title =  anbennar_setup.2.t
	desc =  anbennar_setup.2.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	#fire_only_once = yes
	hidden = yes
	is_triggered_only = yes
	

	
	option = {		
		name = "anbennar_setup.2.a"
		
		# If you're a member of the Rose Party
        if = {
            limit = {
                has_country_flag = lilac_wars_rose_party
            }
			every_country = {	# Rose Party members like each other
				limit = {
					has_country_flag = lilac_wars_rose_party
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = lilac_wars_rose_party_member } 
					}
				}
				add_opinion = { who = ROOT modifier = lilac_wars_rose_party_member }
			}
			every_country = {	# Rose Party members dislike Moon Party
				limit = {
					has_country_flag = lilac_wars_moon_party
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = lilac_wars_rose_party_enemy } 
					}
				}
				add_opinion = { who = ROOT modifier = lilac_wars_rose_party_enemy }
			}
		}
		
		# If you're a member of the Moon Party
        if = {
            limit = {
                has_country_flag = lilac_wars_moon_party
            }
			every_country = {	# Rose Party members like each other
				limit = {
					has_country_flag = lilac_wars_moon_party
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = lilac_wars_moon_party_member } 
					}
				}
				add_opinion = { who = ROOT modifier = lilac_wars_moon_party_member }
			}
			every_country = {	# Rose Party members dislike Moon Party
				limit = {
					has_country_flag = lilac_wars_rose_party
					NOT = { 
						has_opinion_modifier = { who = ROOT modifier = lilac_wars_moon_party_enemy } 
					}
				}
				add_opinion = { who = ROOT modifier = lilac_wars_moon_party_enemy }
			}
		}
	}
}

#Starting discovered territories
country_event = {
	id =  anbennar_setup.3
	title =  anbennar_setup.3.t
	desc =  anbennar_setup.3.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	#fire_only_once = yes
	hidden = yes
	is_triggered_only = yes
	

	
	option = {		
		name = "anbennar_setup.3.a"
		
		#civilized boyos
        if = {
            limit = {
				technology_group = tech_cannorian
            }
			escann_superregion = {
				discover_country = ROOT
			}
			bulwar_superregion = {
				discover_country = ROOT
			}
			western_cannor_superregion = {
				discover_country = ROOT
			}
			gerudia_superregion = {
				discover_country = ROOT
			}
		}
		
        if = {
            limit = {
				OR = {
					technology_group = tech_goblin
					technology_group = tech_orcish
				}
            }
			escann_superregion = {
				discover_country = ROOT
			}
			bulwar_superregion = {
				discover_country = ROOT
			}
			# north_salahad_region = {
				# discover_country = ROOT
			# }
			# the_borders_region = {
				# discover_country = ROOT
			# }
			# esmaria_region = {
				# discover_country = ROOT
			# }
			# east_dameshead_region = {
				# discover_country = ROOT
			# }
			# forlorn_vale_region = {
				# discover_country = ROOT
			# }
			# dameshead_sea_region = {
				# discover_country = ROOT
			# }
		}
		
		# If you're a member of the Moon Party
        if = {
            limit = {
				OR = {
					technology_group = tech_bulwari
					technology_group = tech_salahadesi
				}
            }
			western_cannor_superregion = {
				discover_country = ROOT
			}
			dameshead_sea_region = {
				discover_country = ROOT
			}
			bulwar_superregion = {
				discover_country = ROOT
			}
			salahad_superregion = {
				discover_country = ROOT
			}
		}
	}
}

#Pragmatic Sanction and prob other misc initializers
country_event = {
	id =  anbennar_setup.4
	title =  anbennar_setup.4.t
	desc =  anbennar_setup.4.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	#fire_only_once = yes
	hidden = yes
	is_triggered_only = yes
	

	
	option = {		
		name = "anbennar_setup.4.a"
		
		#civilized boyos
       set_allow_female_emperor = yes
	}
}