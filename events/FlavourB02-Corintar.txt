
namespace = flavor_corintar

#Corintar Reformation
country_event = {
	id = flavor_corintar.0
	title = flavor_corintar.0.t
	desc = flavor_corintar.0.d
	picture = STREET_SPEECH_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		#government_type = adventurer
		#government_rank = 2
	}
	
	option = {		# Despotic Monarchy
		name = "flavor_corintar.0.a"
		ai_chance = { 
			factor = 30
			modifier = {
				factor = 2
				faction_in_power = adv_marchers
			}
		}	
		change_government = despotic_monarchy
		
		#Dynasty name triggers for each country
		hidden_effect = {
			country_event = { id = flavor_corintar.1 days = 1 }
		}
	}
	option = {		# Oligarchic Republic
		name = "flavor_corintar.0.b"
		ai_chance = { 
			factor = 30
			modifier = {
				factor = 1.5
				faction_in_power = adv_pioneers
			}
			modifier = {
				factor = 2	#Having a poor ruler makes it likely they want an elective country
				OR = {
					NOT = { adm = 3 }
					NOT = { dip = 3 }
					NOT = { mil = 3 }
				}
			}
		}
		change_government = oligarchic_republic
	}
	option = {		# Merchant Republic
		name = "flavor_corintar.0.c"
		ai_chance = { 
			factor = 10 
			modifier = {
				factor = 4
				faction_in_power = adv_fortune_seekers
			}
		}
		change_government = merchant_republic
	}
	
	option = {		# Knightly Order
		name = "flavor_corintar.0.e"
		ai_chance = { 
			factor = 30
			modifier = {
				factor = 4
				religion = corinite
			}
		}
		change_government = monastic_order_government
	}	
	
	#Optional
	
	#Damerian Monarchy
	#Magocracy
}

#Dynasty Naming
country_event = {
	id = flavor_corintar.1
	title = dynasty_setup.1.t
	desc = dynasty_setup.1.d
	picture = {
		trigger = {
			NOT = { has_dlc = "Rights of Man" }
		}
		picture = COURT_eventPicture
	}
	picture = {
		trigger = {
			has_dlc = "Rights of Man"
		}
		picture = ROYAL_COUPLE_FUTURE_eventPicture
	}
	
	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		tag = B02
	}
	
	#Option A
	option = {		
		name = "flavor_corintar.1.a"
		ai_chance = { factor = 40 }	
		define_heir = {
			dynasty = "s�l Corin"
			age = 16
			male = yes
			adm = 1
			dip = 1
			mil = 1
			hide_skills = yes
		}
	}
	
	#Option B
	option = {		
		name = "flavor_corintar.1.b"
		ai_chance = { factor = 40 }	
		define_heir = {
			dynasty = "of the Corintar"
			age = 24
			male = yes
			adm = 2
			dip = 2
			mil = 2
			hide_skills = yes
		}
	}
	
	#Keep their name
	option = {		
		name = "flavor_corintar.1.c"
		ai_chance = { factor = 20 }	
		define_heir = {
			dynasty = ROOT
			age = 16
			male = yes
			adm = 1
			dip = 1
			mil = 1
			hide_skills = yes
		}
	}
}

