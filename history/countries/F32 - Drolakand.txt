government = despotic_monarchy
government_rank = 1
primary_culture = brasanni
add_accepted_culture = sun_elf
religion = bulwari_sun_cult
technology_group = tech_bulwari
historical_friend = F05 #historical overlord
capital = 580

1434.1.1 = {
	monarch = {
		name = "Araha I"
		dynasty = "szel-Drolakand"
		birth_date = 1420.11.1
		adm = 2
		dip = 1
		mil = 1
		female = yes
	}
}
