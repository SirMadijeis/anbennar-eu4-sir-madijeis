government = imperial_city
government_rank = 1
mercantilism = 25
primary_culture = crownsman
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 569 #Crothan
fixed_capital = 569

1422.1.1 = { set_country_flag = lilac_wars_rose_party }

1444.1.1 = {
	monarch = {
		name = "City Council"
		adm = 2
		dip = 2
		mil = 2
		regent = yes
	}
}