government = feudal_monarchy
government_rank = 1
primary_culture = esmari
religion = regent_court
technology_group = tech_cannorian
capital = 916 # Seinathil
national_focus = DIP

1422.1.1 = { set_country_flag = lilac_wars_rose_party }

1440.1.12 = {
	monarch = {
		name = "Daran I"
		dynasty = "Silistra"
		birth_date = 1423.8.3
		adm = 0
		dip = 3
		mil = 3
	}
}