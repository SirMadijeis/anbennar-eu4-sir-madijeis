#141 - Serbia

owner = A05
controller = A05
add_core = A05
culture = redfoot_halfling
religion = regent_court
hre = no
base_tax = 6
base_production = 5
trade_goods = cloth
base_manpower = 4
capital = "" 
is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold