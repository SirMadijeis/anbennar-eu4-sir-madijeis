# No previous file for Arkansas
owner = B36
controller = B36
add_core = B36
culture = marrodic
religion = regent_court

hre = no

base_tax = 2
base_production = 2
base_manpower = 3

trade_goods = wool

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish