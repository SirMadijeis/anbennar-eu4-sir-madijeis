# No previous file for Murkglade
owner = Z06
controller = Z06
add_core = Z06
culture = arannese
religion = regent_court

hre = yes

base_tax = 1
base_production = 2
base_manpower = 1

trade_goods = wool

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish