# 71 - Ansbach

owner = A01
controller = A01
add_core = A01
culture = high_lorentish
religion = regent_court
hre = no
trade_goods = wool
capital = "Red Pass"
is_city = yes
base_manpower = 1
base_tax = 1
base_production = 1

fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

